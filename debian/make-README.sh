#!/bin/sh
#
# Simple script to write out README.Debian files for both binary pkg's
#
PKG=$1
ITEM=0

cat <<END
Debian specific notes for the $PKG package
===============================================
END

if [ "$PKG" = "aumix" ]; then 
	ITEM=$(($ITEM + 1))
	cat <<-END

$ITEM. GTK interface to aumix

A GUI interface, based on GTK, is available for aumix. This is packaged as
a separate package "aumix-gtk", because otherwise everyone who would like
to use aumix would have to install the X and GTK libraries. 

Note that the aumix-gtk package has also support for console
(non-graphical operation) and can completely replace the aumix package.

END
fi

ITEM=$(($ITEM + 1))
cat <<-END

$ITEM. Enabling auto save and restore

The package contains an init.d script that can save the mixer settings upon
shutdown, and restore them (as soon as possible) upon booting. If you
want to use this, put

	SAVEMIXER=yes

in /etc/default/aumix. 

In earlier versions, the auto-save option was enabled by default.
However, this clashes with other packages (e.g. setmixer) that also save, mute
and restore.  Therefore, this feature has been disabled by default from
version 2.8-4 on.

In previous versions, the presence of the file /etc/default/aumix.stop
was used to determine that you don't want the mixer settings to be
saved/restored. This is still supported, but will trigger a warning
message.

END
ITEM=$(($ITEM + 1))
cat <<-END

$ITEM. Suppressing modprobe error messages

Note that you may see error messages like this: "modprobe: modprobe:
Can't locate module sound-slot-1" on the console at start up, or in
/var/log/syslog.  This is caused by /etc/init.d/aumix trying to load
mixer settings for all /dev/mixer* devices.  Unless your system is using
devfs, you will probably have more /dev/mixer* device nodes than
physical mixer devices in your system.

Suppressing modprobe error messages for nonexistent mixer devices is
simply a matter of creating dummy aliases for those modules.  On Debian
systems, these aliases go in files under the /etc/modutils directory.
If you already have a file containing sound or other module definitions
for your system, add them there.  Otherwise, create a file called
	/etc/modutils/sound-dummy and add them there.

For example, if the error message complains about sound-slot-1 and
sound-service-1-0, you would add the following to the file:

        alias sound-slot-1 off
        alias sound-service-1-0 off

Replace the module names above with the names reported on your system.
After saving the file, run "update-modules" once.  (All of this must be
done as root.)

As mentioned above, systems using the devfs filesystem don't have this
problem, since devfs dynamically creates device nodes in the filesystem
for devices actually present. For more information about this, see
Documentation/filesystems/devfs/README in your Linux kernel source or 
kernel-doc-2.4.18 package.

END
ITEM=$(($ITEM + 1))
cat <<-END

$ITEM. Notes for ALSA users

If you also have the Debian ALSA packages installed, the aumix init.d
script will detect this and refuse to touch the mixer, as this is
handled by the alsactl program and the /etc/init.d/alsa script instead.

If you don't wish to use the ALSA stuff, e.g. since you only access ALSA
in OSS emulation mode, you can forbid aumix execution at boot/shutdown
by editing /etc/defaults/aumix. See the first note in this file for more
information.
  
The old aumix-alsa package has been removed as there is no support for
it anymore upstream.  The aumix package works fine (although with
limited channels) with the OSS compatibility layer for ALSA.  To enable
the compatibility layer, modprobe the snd_mixer_oss module (and/or put
it in /etc/modules).

Hopefully aumix will have full ALSA support in the future (it is on
upstream's TODO list).  If you're knowledgeable with ALSA stuff, feel
free to contact aumix@packages.debian.org to help out.  The Debian
maintainer works closely with the aumix upstream maintainer.

END

if [ "$PKG" = "aumix-gtk" ]; then 
	ITEM=$(($ITEM + 1))
	cat <<-END

	$ITEM. /etc/init.d/alsa-gtk

	Starting with version 2.7-24, aumix and aumix-gtk use the same
	init.d file /etc/init.d/aumix.  Earlier version of aumix-gtk used
	/etc/init.d/aumix-gtk instead.  On upgrade, the old
	/etc/init.d/aumix-gtk file was renamed to /etc/init.d/aumix.  The
	old /etc/init.d/aumix file, if it existed, was saved as
	/etc/init.d/aumix.dpkg-old.

END
fi

cat <<END
        Bas Zoetekouw <bas@debian.org>
        Eduard Bloch <blade@debian.org>
END
