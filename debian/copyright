Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: auxmix
Upstream-Contact: Trevor Johnson <trevor@jpj.net>
Source: https://sourceforge.net/projects/aumix/

Files: *
Copyright: 1993, 1996-2002, 2004, 2005, 2008, 2010, the authors-see AUTHORS file
License: GPL-2+
Comment: The Authors list, as in version 2.9.1, shows the following:
 Savio Lam <lam836@cs.cuhk.hk> 
 TimeCop <timecop@japan.co.jp>
 Trevor Johnson <trevor@jpj.net>
 Alessandro Rubini <alessandro.rubini@linux.it> 
 Miguel de Icaza <miguel@roxanne.nuclecu.unam.mx> 
 Kenn Humborg <kenn@wombat.ie> 
 Dan Fandrich <dan@fch.wimsey.bc.ca> 
 Jeff Spirko <spirko@topquark.physics.lehigh.edu> 
 Harley Silver <harley@widow.aracnet.net> 
 Massachusetts Institute of Technology 
 Rainer Canavan <canavan@cs.bonn.edu> 
 Jim Diamond <diamond@atl.sofkin.ca> 
 Holger Kiel <hk@tilt.prima.de> 
 Philip Chong <pchong@eecs.berkeley.edu> 
 G. Brubaker <xavyer@ix.netcom.com> 
 Jonathan H. Aseltine <aseltine@cs.umass.edu> 
 Arkadiusz Mi¶kiewicz <misiek@misiek.eu.org> 
 Bill Nottingham <notting@redhat.com> 
 David Reviejo <dreviejo@arrakis.es> 
 Johnny Teveßen <j.tevessen@line.org> 
 Arnaldo Carvalho de Melo <acme@conectiva.com.br> 
 Pablo Ruiz Garcia <dr_pci@spunfa.dhis.org>
 Michael Vasilenko <acid@stu.cn.ua>, also known as Grisha Vasiliev
 Michal Svec <rebel@atrey.karlin.mff.cuni.cz> 
 Michael Fulbright <msf@redhat.com> 
 Erik Troan <ewt@redhat.com> 
 Otto Hammersmith <otto@redhat.com> 
 Cristian Gafton <gafton@redhat.com> 
 Prospector System <bugs@redhat.com> 
 Eric Wanner <ericw@futureone.com> 
 Ernesto Hernández-Novich <emhn@telcel.net.ve> 
 Valery Kornienkov <vlk@dimavb.simbirsk.su> 
 Caldera Systems <info@calderasystems.com> 
 Chris Piazza <cpiazza@freebsd.org> 
 Robert Siemer <siemer@i309.hadiko.de> 
 Paul Slootman <paul@debian.org> 
 Jesús Bravo Álvarez <jba@pobox.com> 
 Christian Weisgerber <naddy@openbsd.org>
 Owen Taylor <otaylor@redhat.com> 
 Piotr Czerwiñski <pius@pld.org.pl> 
 Dominik Stadler <dominik.stadler@gmx.at> 
 Pablo Saratxaga <pablo@mandrakesoft.com> 
 Roland Rosenfeld <roland@spinnaker.de> 
 Tom Tromey <tromey@cygnus.com> 
 Pete Hollobon <hollobon@bigfoot.com> 
 Sergey Kiselev <sergey@junior.technion.ac.il> 
 Aaron McDaid <hoss@technologist.com> 
 Tony Gale <gale@gtk.org> 
 Ian Main <imain@gtk.org> 
 Guillaume Cottenceau <gc@mandrakesoft.com> 
 Venkatesh Krishnamurthi <vk@spies.com> 
 Ulrich Drepper <drepper@redhat.com> 
 Masanori Hanawa 
 Takagi Junji 
 Kimiyasu Tomiyama <tomy@truga.com> 
 Magnus Reftel <d96reftl@dtek.chalmers.se> 
 Anders Widell <d95-awi@nada.kth.se> 
 Christer Gustavsson <f93-cgn@sm.luth.se> 
 Karl Backström <backstrom@kde.org> 
 Wang Jian <lark@linux.net.cn> 
 Ben Ford <ben@kalifornia.com> 
 Jelmer Vernooij <jelmer@nl.linux.org> 
 Olexander Kunytsa <kunia@istc.kiev.ua> 
 Mikael Hedin <mikael.hedin@irf.se> 
 Takeshi Aihana <aihana@turbolinux.co.jp> 
 Eric S. Raymond <esr@golux.thyrsus.com> 
 Nalin Dahyabhai <nalin@redhat.com> 
 Cyro Mendes de Moraes Neto <neto@conectiva.com.br> 
 Osvaldo Santana Neto <osvaldo@conectiva.com.br> 
 Martin J. Hiller <soliton@catbull.com> 
 Sofia Pappatheodorou <sofia@csudh.edu> 
 Bas Zoetekouw <bas@a-eskwadraat.nl> 
 Ivan Tonizza <alphanum@libero.it> 
 Roland Stigge <stigge@antcom.de> 
 Thierry Vignaud <tvignaud@mandrakesoft.com> 
 Götz Waschk <waschk@linux-mandrake.com> 
 Ernest Adrogué <eadrogue@gmx.net> 
 Cristian Rigamonti <bigamons@freemail.it> 
 Pixel <pixel@mandrakesoft.com> 
 Bruce Sass <bsass@evsmail.com> 
 Michael Lee Yohe <michael@yohe.net> 
 Kees Cook <kees@ubuntu.com> 
 Aleksey Cheusov <cheusov@tut.by> 
 Mikael Hedin <micce@debian.org> 
 Bruno Haible <haible@clisp.cons.org> 
 Free Software Foundation 
 Gordon Matzigkeit <gord@gnu.ai.mit.edu> 
 Romain Francoise <rfrancoise@debian.org> 
 Cory Wiltshire <wiltshi@cs.montana.edu> 
 Owen Leonard 
 Sébastien Hinderer <Sebastien.Hinderer@ens-lyon.org> 
 Tibor Csögör <tibi@tiborius.net> 
 Stefan Ott <stefan@ott.net> 
 Bernhard Reiter <bernhard@intevation.de> 
 Eduard Bloch <blade@debian.org> 
 Marcus Meissner <meissner@suse.de> 

Files: build-aux/*
Copyright: 1999, 2000, 2003-2007, Free Software
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: Expat

Files: build-aux/missing
Copyright: 1996, 1997, 1999, 2000, 2002-2006, Free Software Foundation, Inc.
License: GPL-2+

Files: configure
Copyright: 1992-1996, 1998-2008, Free Software Foundation, Inc.
License: FSF.Unlimited.License
 This configure script is free software; the Free Software Foundation
 gives unlimited permission to copy, distribute and modify it.

Files: debian/xaumix
Copyright: 1999, 2000, Paul Slootman <paul@debian.org>
License: GPL
Comment: There is no version specified for the GPL license.

Files: src/dummy.c
Copyright: 1999, Christian Weisgerber
License: GPL-2+

Files: src/gpm-xterm.c
Copyright: 1994, Janne Kukonlehto
  1994, 1995, Alessandro Rubini <rubini@linux.it>
License: GPL-2+

Files: src/gpm-xterm.h
Copyright: 1994, Miguel de Icaza <miguel@roxanne.nuclecu.unam.mx>
  1994, 1995, Alessandro Rubini <rubini@ipvvis.unipv.it>
License: GPL-2+

Files: src/gtk.c
Copyright: 1998, Pete Hollobon <hollobon@bigfoot.com>
  1998, 1999, Sergey Kiselev
  1998-2001, the authors (see AUTHORS file)
License: GPL-2+

Files: src/mute
Copyright: 2001, Ben Ford and Trevor Johnson
License: GPL-2+
Comment: Assuming the project GPL-2+ license.

Files: src/xaumix
Copyright: 1999, Paul Slootman
License: GPL-2+

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
